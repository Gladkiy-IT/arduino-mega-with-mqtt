String topicLog = "/home/dev1/log";

String topicPWMFreq = "/home/dev1/setPWMFreq";
String topicPWMPresc = "/home/dev1/setPWMPresc";

String topicStates = "/home/dev1/sendStates";
String topicOffAll = "/home/dev1/offAll";
String topicRestablishAll = "/home/dev1/restablishAll";

String topicsRelay[quantityPinsRelay] = {
"/home/dev1/rele1",
"/home/dev1/rele2",
"/home/dev1/rele3",
"/home/dev1/rele4",
"/home/dev1/rele5",
"/home/dev1/rele6",
"/home/dev1/rele7",
//"/home/dev1/rele8"
};

const uint8_t quantityPWM = 16;

String topicsPWM[quantityPWM] = {
"/home/dev1/pwm1",
"/home/dev1/pwm2",
"/home/dev1/pwm3",
"/home/dev1/pwm4",
"/home/dev1/pwm5",
"/home/dev1/pwm6",
"/home/dev1/pwm7",
"/home/dev1/pwm8",
"/home/dev1/pwm9",
"/home/dev1/pwm10",
"/home/dev1/pwm11",
"/home/dev1/pwm12",
"/home/dev1/pwm13",
"/home/dev1/pwm14",
"/home/dev1/pwm15",
"/home/dev1/pwm16"
};

String topicsPWM100[quantityPWM] = {
"/home/dev1/pwm1/to100",
"/home/dev1/pwm2/to100",
"/home/dev1/pwm3/to100",
"/home/dev1/pwm4/to100",
"/home/dev1/pwm5/to100",
"/home/dev1/pwm6/to100",
"/home/dev1/pwm7/to100",
"/home/dev1/pwm8/to100",
"/home/dev1/pwm9/to100",
"/home/dev1/pwm10/to100",
"/home/dev1/pwm11/to100",
"/home/dev1/pwm12/to100",
"/home/dev1/pwm13/to100",
"/home/dev1/pwm14/to100",
"/home/dev1/pwm15/to100",
"/home/dev1/pwm16/to100"
};


String topicsStatesPWM[quantityPWM] = {
"/home/dev1/pwm1/State",
"/home/dev1/pwm2/State",
"/home/dev1/pwm3/State",
"/home/dev1/pwm4/State",
"/home/dev1/pwm5/State",
"/home/dev1/pwm6/State",
"/home/dev1/pwm7/State",
"/home/dev1/pwm8/State",
"/home/dev1/pwm9/State",
"/home/dev1/pwm10/State",
"/home/dev1/pwm11/State",
"/home/dev1/pwm12/State",
"/home/dev1/pwm13/State",
"/home/dev1/pwm14/State",
"/home/dev1/pwm15/State",
"/home/dev1/pwm16/State"
};

String topicsOnOffsPWM[quantityPWM] = {
"/home/dev1/pwm1/onoff",
"/home/dev1/pwm2/onoff",
"/home/dev1/pwm3/onoff",
"/home/dev1/pwm4/onoff",
"/home/dev1/pwm5/onoff",
"/home/dev1/pwm6/onoff",
"/home/dev1/pwm7/onoff",
"/home/dev1/pwm8/onoff",
"/home/dev1/pwm9/onoff",
"/home/dev1/pwm10/onoff",
"/home/dev1/pwm11/onoff",
"/home/dev1/pwm12/onoff",
"/home/dev1/pwm13/onoff",
"/home/dev1/pwm14/onoff",
"/home/dev1/pwm15/onoff",
"/home/dev1/pwm16/onoff"
};

String topicsOnOffStatesPWM[quantityPWM] = {
"/home/dev1/pwm1/onoffState",
"/home/dev1/pwm2/onoffState",
"/home/dev1/pwm3/onoffState",
"/home/dev1/pwm4/onoffState",
"/home/dev1/pwm5/onoffState",
"/home/dev1/pwm6/onoffState",
"/home/dev1/pwm7/onoffState",
"/home/dev1/pwm8/onoffState",
"/home/dev1/pwm9/onoffState",
"/home/dev1/pwm10/onoffState",
"/home/dev1/pwm11/onoffState",
"/home/dev1/pwm12/onoffState",
"/home/dev1/pwm13/onoffState",
"/home/dev1/pwm14/onoffState",
"/home/dev1/pwm15/onoffState",
"/home/dev1/pwm16/onoffState"
};


//String topicsPWMup[quantityPWM] = {
//"/home/dev1/pwm1up",
//"/home/dev1/pwm2up",
//"/home/dev1/pwm3up",
//"/home/dev1/pwm4up",
//"/home/dev1/pwm5up",
//"/home/dev1/pwm6up",
//"/home/dev1/pwm7up",
//"/home/dev1/pwm8up"
//};
//
//String topicsPWMdown[quantityPWM] = {
//"/home/dev1/pwm1down",
//"/home/dev1/pwm2down",
//"/home/dev1/pwm3down",
//"/home/dev1/pwm4down",
//"/home/dev1/pwm5down",
//"/home/dev1/pwm6down",
//"/home/dev1/pwm7down",
//"/home/dev1/pwm8down"
//
//};
//
//String topicsPWMchange[quantityPWM] = {
//"/home/dev1/pwm1change",
//"/home/dev1/pwm2change",
//"/home/dev1/pwm3change",
//"/home/dev1/pwm4change",
//"/home/dev1/pwm5change",
//"/home/dev1/pwm6change",
//"/home/dev1/pwm7change",
//"/home/dev1/pwm8change"
//};

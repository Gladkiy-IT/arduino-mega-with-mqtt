//Выводы mega, которые лучше не занимать чем попало
//Интерфейс SPI: выводы 50 (MISO), 51 (MOSI), 52 (SCK), 53 (SS)
//Последовательный интерфейс Serial: выводы 0 (RX) и 1 (TX);
//Внешние прерывания: выводы 2 (прерывание 0), 3 (прерывание 1), 18 (прерывание 5), 19 (прерывание 4), 20 (прерывание 3) и 21 (прерывание 2)
//ШИМ: выводы 2 - 13 и 44 - 46
//TWI: выводы 20 (SDA) и 21 (SCL) (1wire)

#define DEBBUG 1

#ifdef DEBUG
#define DEBUG_ONLY(x) x
#else
#define DEBUG_ONLY(x)
#endif


#define USE_MQTT  1        //использовать MQTT


#include "timerMinim.h"

#include "tablePWM.h"


#if (USE_MQTT == 1)
#include <Ethernet.h>
#endif

#if (USE_MQTT == 1)
#include <PubSubClient.h>
timerMinim reconnectMQTT(60000);
#endif


#include "GyverButton.h"      
#include "GladRelay.h" 
#include "GladPWM.h" 
#include <SPI.h>

#define STEP_TIMEOUT  25

GladPWM pwm(1000);

const uint8_t quantityPinsPWM = 8;
GButton pinsPWMOn[quantityPinsPWM] =  {A0, A2, A4, A6, A8, A10, A12, A14};
GButton pinsPWMOff[quantityPinsPWM] = {A1, A3, A5, A7, A9, A11, A13, A15};

uint16_t debouncePWMValue = 4; 
uint16_t debouncePWM[quantityPinsPWM];



bool changePWM = false;

const uint8_t quantityPinsPWMSwitch = 5;
GButton pinsPWMSwitch[quantityPinsPWMSwitch] =  {22, 23, 24, 25, 26};
const uint8_t pwmSwitchChannels[quantityPinsPWMSwitch] = {7, 9, 10, 11, 12};//13, 14, 15

unsigned long pwmSwitchTime[quantityPinsPWMSwitch] = {0,0,0};
unsigned long timeMod = 2000;


const uint8_t quantityPinsRelay = 7;
GladRelay pinsOutRelay[quantityPinsRelay] =   {42, 43, 44, 45, 46, 47, 48}; //, 49};
GButton pinsRelayOn[quantityPinsRelay] =      {28, 30, 32, 34, 36, 38, 40};
GButton pinsRelayOff[quantityPinsRelay] =     {29, 31, 33, 35, 37, 39, 41}; 
//27 пин не работает почему-то, заменен на 39

/*
const uint8_t quantitySinglePinsRelay = 3;
GladRelay pinsSingleOutRelay[quantitySinglePinsRelay] = {47, 48, 49};
GButton pinsSingleRelay[quantitySinglePinsRelay] = {32, 34, 36};
*/

#include "mqtt_topics.h"




//char* PinsChar[] = {"0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"};


GButton PinOffAll = 6;
GButton PinReestablishAll = 7;



byte mac[]    = {  0xDE, 0xED, 0xBA, 0xFE, 0xFE, 0xED };
IPAddress server(192, 168, 1, 77);
IPAddress ip(192, 168, 1, 235);
IPAddress myDNS(192, 168, 1, 1);

EthernetClient ethClient;





void initializeButtons();
void initializeOutputs();
void initializeEthernet();
void initializeMQTT();
void sendAllStates();
void sendStatePWM(uint8_t i);
void sendStateRele(uint8_t i);
void offAll();


void restablishAll();


#if (USE_EEPROM == 1)
void writeToFlashRele(uint8_t num, bool val);
bool readFromFlashRele(uint8_t num);
void writeToFlashPWM(uint8_t num, uint16_t val);
uint16_t readFromFlashPWM(uint8_t num);
void readStatesFromFlash();
void  writePWMStatesToFlash();
#endif


unsigned char  sendStates[5]; 
unsigned char mqttMessage[128]; 
void callback(char* topic, unsigned char* payload, unsigned int length);


void relayOffClick(uint8_t i);
void relayOnClick(uint8_t i);
//void relayOnClick(uint8_t i);

void pwmSwitchSingleClick(uint8_t i);
void pwmSwitchDoubleClick(uint8_t i);
void pwmSwitchHolded(uint8_t i);


void pwmOnSingleClick(uint8_t i);
void pwmOnDoubleClick(uint8_t i);
void pwmOnHolded(uint8_t i);
void pwmOnStep(uint8_t i);
void pwmOffSingleClick(uint8_t i);
void pwmOffDoubleClick(uint8_t i);
void pwmOffHolded(uint8_t i);
void pwmOffStep(uint8_t i);


int num = 0;
boolean findtopic = false;

void callback(char* topic, unsigned char* payload, unsigned int length) {
  //unsigned char* p = (unsigned char*)malloc(length+1);  
  memcpy(mqttMessage,payload,length);
  mqttMessage[length]="\0";
  num = atoi(mqttMessage);
  findtopic = false;

  if(!findtopic && strcmp(topic, topicStates.c_str()) == 0 )
  {
    findtopic = true;
    sendAllStates();
    Serial.println(topicStates);
  }
  if(!findtopic && strcmp(topic, topicOffAll.c_str()) == 0 )
  {
    findtopic = true;
    offAll();
    sendAllStates();
    Serial.println(topicOffAll);
  }
  if(!findtopic && strcmp(topic, topicRestablishAll.c_str()) == 0 )
  {
    findtopic = true;
    restablishAll();
    sendAllStates();
    Serial.println(topicRestablishAll);
  }
  if(!findtopic && strcmp(topic, topicPWMFreq.c_str()) == 0 )
  {
    findtopic = true;
    if(num>0 && num <=1600)
    pwm.setPWMFreq(num);
    Serial.print("new freq: ");
    Serial.println(num);
  }
  if(!findtopic && strcmp(topic, topicPWMPresc.c_str()) == 0 )
  {
    findtopic = true;
    if(num>=0 && num <=255)
    pwm.setExtClk(num);
    Serial.print("new freq: ");
    Serial.println(num);
  }
      
  if(!findtopic && strstr(topic, "rele") != NULL)
  {
    for (int i=0; i<quantityPinsRelay; ++i)
    {
      if(strcmp(topic, topicsRelay[i].c_str()) == 0 )
      {
        findtopic = true;
        Serial.println("Rele");
        if(num == 0)
        {
          pinsOutRelay[i].off();
          Serial.println("Rele off");
        }
        else if(num == 1)
        {
          pinsOutRelay[i].on();
          Serial.println("Rele on");
        }
        else 
        {
          Serial.println("Value not recognized");
        }
        break;
      }
    }
  }

  if(!findtopic && strstr(topic, "pwm") != NULL)
  {
    Serial.println("pwm");
    /*
    if(!findtopic && strstr(topic, "up") != NULL)
    {
      Serial.println("pwm up");
      for (int i=0; i<quantityPinsPWM; ++i)
      {
        if(strcmp(topic, topicsPWMup[i].c_str()) == 0 )
        {
          findtopic = true;
          Serial.println(pwm.getState(i));
          pwm.up(i,1);
          Serial.println(pwm.getState(i));
          sendStatePWM(i); 
          break;
        }
      }        
    }
    if(!findtopic && strstr(topic, "down") != NULL)
    {
      Serial.println("pwm down");
      for (int i=0; i<quantityPinsPWM; ++i)
      {
        if(strcmp(topic, topicsPWMdown[i].c_str()) == 0 )
        {
          findtopic = true;
          Serial.println(pwm.getState(i));
          pwm.down(i,1);
          Serial.println(pwm.getState(i));
          sendStatePWM(i);
          break;
        }
      }        
    }    

    if(!findtopic && strstr(topic, "change") != NULL)
    {
      Serial.println("pwm down");
      for (int i=0; i<quantityPinsPWM; ++i)
      {
        if(strcmp(topic, topicsPWMchange[i].c_str()) == 0 )
        {
          findtopic = true;
          Serial.println(pwm.getState(i));
          pwm.changeMode(i);
          Serial.println(pwm.getState(i));
          sendStatePWM(i);
          break;
        }
      }        
    }  
    */

    if(!findtopic && strstr(topic, "onoff") != NULL)
    {
      Serial.println("pwm up");
      for (int i=0; i<quantityPWM; ++i)
      {
        if(strcmp(topic, topicsOnOffsPWM[i].c_str()) == 0 )
        {
          findtopic = true;
          if(num)
            pwm.on(i);
          else
            pwm.off(i);
            
          sendStatePWM(i); 
          break;
        }
      }        
    }
    if(!findtopic)
    {
      Serial.println("pwm set");
      for (int i=0; i<quantityPWM; ++i)
      {
        if(strcmp(topic, topicsPWM[i].c_str()) == 0 )
        {
          findtopic = true;
          pwm.setState(i, num);
          sendStatePWM(i);
          break;
        }
      }      
    }

  }

  if(!findtopic)
  {
    Serial.println("Topic not found");
  }
}

PubSubClient client(server, 1883, callback, ethClient);



void setup()
{

  Serial.begin(115200);
  Serial.println("startSerial");
  pwm.init();
  Serial.println("startPWM");
  initializeOutputs();
  Serial.println("startOutputs");
  initializeButtons();
  Serial.println("startButtons");

  
  Serial.println("startBTN");

  #if (USE_MQTT ==1)
    initializeEthernet();
    initializeMQTT();
  #endif
  Serial.println("startMQTT");
  for(uint8_t i=0; i<quantityPinsRelay; ++i)
  {
    pinsOutRelay[i].init();
    sendStateRele(i);
  }
Serial.println("startRelay");
client.publish(topicLog.c_str(), "start");
Serial.println("starting loop");
}

//LOOP

void loop()
{
//  client.loop();
  if(!client.loop())
  {
    if(reconnectMQTT.isReady())
    {
//      DEBUG_ONLY(Serial.println("Init MQTT"));
      initializeMQTT();
    }
  }

  
  
  for(uint8_t i=0; i<quantityPinsRelay; ++i)
  {
    if (pinsRelayOn[i].isClick() || pinsRelayOn[i].isHolded())
    {      
      relayOnClick(i);
      client.publish(topicLog.c_str(), "on");

    }
    if (pinsRelayOn[i].isDouble())
    {
      relayOnDoubleClick(i);
      client.publish(topicLog.c_str(), "double");

    }
    if (pinsRelayOff[i].isClick() || pinsRelayOff[i].isHolded() )
    {
      relayOffClick(i);      
    }      
  }





  for(uint8_t i=0; i<quantityPinsPWMSwitch; ++i)
  {
    if (pinsPWMSwitch[i].isSingle())
    {
      pwmSwitchSingleClick(i);
   
    }
    if (pinsPWMSwitch[i].isDouble())
    {
         pwmSwitchDoubleClick(i);  
    }
    if (pinsPWMSwitch[i].isHold() || pinsPWMSwitch[i].isHolded())
    {
        pwmSwitchHolded(i);     
    }
            
  }

    

  for(uint8_t i=0; i<quantityPinsPWM; ++i)
  {
    if (pinsPWMOn[i].isSingle())
    {
      pwmOnSingleClick(i);
    }    
    if (pinsPWMOff[i].isSingle())
    {
      pwmOffSingleClick(i);
    }
    if (pinsPWMOn[i].isDouble())
    { 
      pwmOnDoubleClick(i);      
    }
    if (pinsPWMOff[i].isDouble())
    { 
      pwmOffDoubleClick(i);
    }
    if (pinsPWMOn[i].isStep())
    {
//      pwmOnHolded(i);
       pwmOnStep(i);
    }
    if (pinsPWMOff[i].isStep())
    {
//      pwmOffHolded(i);
      pwmOffStep(i);
    }  
  }
  
  if (PinOffAll.isDouble() || PinOffAll.isHolded())
  {
    offAll();
  }
  
  
  if (PinReestablishAll.isClick() || PinReestablishAll.isHolded())
  {
    restablishAll();
  }

  
//  if (PinOffAll.isDouble())
//  {
//    offAll();
//    pwm.setMinAll();   
//  }

  if (PinReestablishAll.isDouble())
  {
    pwm.setMaxAll();
    
    for(uint8_t i=0; i<quantityPinsRelay; ++i)
    {
      pinsOutRelay[i].on();
      sendStateRele(i);
    }
  }
 
  
  
}



//==============================================================================================================================
void relayOffClick(uint8_t i)
{
      pinsOutRelay[i].off();
      client.publish(topicsRelay[i].c_str(),"0");
      #if (USE_EEPROM ==1)
        writeToFlashRele(i, 0);
      #endif
      sendStateRele(i);
      Serial.print(i);
      Serial.println("  OFF");   
}
void relayOnClick(uint8_t i)
{
      pinsOutRelay[i].on();
      client.publish(topicsRelay[i].c_str(),"1");
      #if (USE_EEPROM ==1)
        writeToFlashRele(i, 1);
      #endif
      sendStateRele(i);
      Serial.print(i);
      Serial.println(" ON");  
}
void relayOnDoubleClick(uint8_t i)
{
      pinsOutRelay[i].offAfter(1*60*60*1000);
      client.publish(topicsRelay[i].c_str(),"1");
      #if (USE_EEPROM ==1)
        writeToFlashRele(i, 1);
      #endif
      sendStateRele(i);
      Serial.print(i);
      Serial.println(" ON");  
}

//=====================================================================

void pwmSwitchSingleClick(uint8_t i)
{
      
      if (millis()-pwmSwitchTime[i]>timeMod)
      {
        if(pwm.getState(pwmSwitchChannels[i])==0)
        {
          pwm.on(i);
//          pwm.changeMode(pwmSwitchChannels[i]);
        }
        else
        {
          pwm.off(pwmSwitchChannels[i]);
        }      
      }
      else
      {
        pwm.changeMode(pwmSwitchChannels[i]);
      }
    
      sendStatePWM(pwmSwitchChannels[i]);
      changePWM = true; 
      pwmSwitchTime[i] = millis();  
}

void pwmSwitchDoubleClick(uint8_t i)
{
      if (millis()-pwmSwitchTime[i]>timeMod)
      {
        pwm.changeMode(pwmSwitchChannels[i]);
      }
      else
      {
        pwm.setMax(pwmSwitchChannels[i]);
      }      
      
      sendStatePWM(pwmSwitchChannels[i]);
      changePWM = true;
      pwmSwitchTime[i] = millis();
}
void pwmSwitchHolded(uint8_t i)
{
      if (millis()-pwmSwitchTime[i]>timeMod)
      {
        pwm.setMin(pwmSwitchChannels[i]);
      }
      else
      {
        pwm.off(pwmSwitchChannels[i]); 
      }         

        sendStatePWM(pwmSwitchChannels[i]);
        changePWM = true;
        pwmSwitchTime[i] = millis();
}

//===============================================

void pwmOnSingleClick(uint8_t i)
{
      if(pwm.getState(i)==0)
      {
        pwm.on(i);
      }
      else
      {
        Serial.println(pwm.getState(i));
        pwm.changeMode(i);
        Serial.println(pwm.getState(i));
        
      }
      sendStatePWM(i);
      changePWM = true;   
}
void pwmOnDoubleClick(uint8_t i)
{
      pwm.setMax(i);
      sendStatePWM(i);
      changePWM = true;   
}
void pwmOnHolded(uint8_t i)
{
      ++debouncePWM[i];
      if (debouncePWM[i] > 2*debouncePWMValue)
      {
        debouncePWM[i] = debouncePWMValue;  
        pwm.up(i,5000);
        sendStatePWM(i);
        changePWM = true; 
      }      
}
void pwmOnStep(uint8_t i)
{
  pwm.up(i,5000);
  sendStatePWM(i);   
}
void pwmOffSingleClick(uint8_t i)
{
      if(pwm.getState(i)!=0)
      {
        pwm.off(i); 
      }
      else
      {
        pwm.setMin(i); 
      }
      sendStatePWM(i); 
      changePWM = true;
}
void pwmOffDoubleClick(uint8_t i)
{
      pwm.setMin(i); 
      sendStatePWM(i);
      changePWM = true;   
}

void pwmOffHolded(uint8_t i)
{
      --debouncePWM[i];
      if (debouncePWM[i] ==0)
      {
        debouncePWM[i] = debouncePWMValue;     
        pwm.down(i,5000);
        sendStatePWM(i);
        changePWM = true; 
      }
}
void pwmOffStep(uint8_t i)
{
  pwm.down(i,5000);
  sendStatePWM(i);   
}




void sendAllStates()
{
    for (uint8_t i = 0; i < quantityPinsRelay; ++i)
  {
    sendStateRele(i);
  }
  for (uint8_t i = 0; i < quantityPWM; ++i)
  {
    sendStatePWM(i);
  }  
}
void sendStatePWM(uint8_t i)
{
    itoa(pwm.getState(i), sendStates, 10);
    client.publish(topicsStatesPWM[i].c_str(),sendStates);
    if(pwm.getState(i) > 0)
      client.publish(topicsOnOffStatesPWM[i].c_str(),"1");
    else
      client.publish(topicsOnOffStatesPWM[i].c_str(),"0");
    
}
void sendStateRele(uint8_t i)
{
    if(pinsOutRelay[i].getState())
      client.publish(topicsRelay[i].c_str(), "1");
    else
      client.publish(topicsRelay[i].c_str(), "0");     
}



void offAll()
{
  for (uint8_t i = 0; i < quantityPinsRelay; ++i)
  {
    pinsOutRelay[i].off(true);
  }
  pwm.offAll();
  changePWM = true;
}
void restablishAll()
{
  for (uint8_t i = 0; i < quantityPinsRelay; ++i)
  {
    pinsOutRelay[i].setOldState();
  }
  pwm.restablishAll();
  changePWM = true;
}





void initializeButtons()
{
  for(int i=0;i<quantityPinsPWM;++i)
  {
      pinsPWMOn[i].setTickMode(AUTO);
      pinsPWMOff[i].setTickMode(AUTO);
      pinsPWMOff[i].setStepTimeout(STEP_TIMEOUT);
      pinsPWMOn[i].setStepTimeout(STEP_TIMEOUT);
      debouncePWM[i] = debouncePWMValue;
  }
  for(int i=0;i<quantityPinsPWMSwitch;++i)
  {
      pinsPWMSwitch[i].setTickMode(AUTO);
  }
  for(uint8_t i=0; i<quantityPinsRelay; ++i)
  {
      pinsRelayOn[i].setTickMode(AUTO);
      pinsRelayOff[i].setTickMode(AUTO);
  }
  PinOffAll.setTickMode(AUTO);
  PinReestablishAll.setTickMode(AUTO);  
}


void initializeOutputs()
{
  for(uint8_t i=0; i<quantityPinsRelay; ++i)
  {
    pinsOutRelay[i].init();
    pinsOutRelay[i].off();
  }  
}

void initializeEthernet()
{
  Ethernet.init(53);
//  Serial.println("Initialize Ethernet with DHCP:");
//  if (Ethernet.begin(mac) == 0)  
//  {
//    Serial.println("Failed to configure Ethernet using DHCP");
//    if (Ethernet.hardwareStatus() == EthernetNoHardware) 
//    {
//      Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
//    } else if (Ethernet.linkStatus() == LinkOFF) 
//    {
//      Serial.println("Ethernet cable is not connected.");
//    }
//    // no point in carrying on, so do nothing forevermore:
//  }
  // print your local IP address:
  Ethernet.begin(mac, ip, myDNS);
  Serial.print("My IP address: ");
  Serial.println(Ethernet.localIP());  
}

//
void initializeMQTT()
{
  DEBUG_ONLY(Serial.println("Init MQTT"));
  if (client.connect("arduinoClient")) 
  {
    client.publish("/home/dev1/status","online");
    sendAllStates();
    client.subscribe(topicStates.c_str());
    client.subscribe(topicOffAll.c_str());
    client.subscribe(topicRestablishAll.c_str());
    client.subscribe(topicPWMFreq.c_str());
    client.subscribe(topicPWMPresc.c_str());
    
       
    for(int i=0; i<quantityPinsRelay; ++i)
    {
      client.subscribe(topicsRelay[i].c_str());
    }
    for(int i=0; i<quantityPWM; ++i)
    {
      client.subscribe(topicsPWM[i].c_str());
      client.subscribe(topicsOnOffsPWM[i].c_str());

//      client.subscribe(topicsPWMdown[i].c_str());
//      client.subscribe(topicsPWMup[i].c_str());
//      client.subscribe(topicsPWMchange[i].c_str());
    }    
  } 
}

//=============================================
//callback




//==============================================================================
//
